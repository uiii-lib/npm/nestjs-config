# Config module for NestJS

Creates a service for accessing enviroment variables. Also loads variables from `.env` file if present.

This module also exposes CRA-specific enviroment variables (with a `REACT_APP_` prefix) for a client. See [Usage (client)](#usage-client)

## Configuration

Environment variables:

- `CONFIG_PUBLIC_PATH` (*optional*) - path used to place file to publish environament variables to the client, see [Usage (client)](#usage-client)

## Usage (server)

Register `@uiii-lib` package registry to npm

```
npm config set @uiii-lib:registry https://gitlab.com/api/v4/packages/npm/
```

Install the package

```
npm install --save @uiii-lib/nestjs-config
```

Add config module to `imports` of a module which you want to use it in.

```ts
import { ConfigModule } from '@uiii-lib/nestjs-config';

@Module({
	imports: [
		ConfigModule
	],
	...
})
export class SomeModule
```

Create a module-specific config

```ts
import { Injectable } from '@nestjs/common';

import { ConfigService } from '@uiii-lib/nestjs-config';

@Injectable()
export class SomeModuleConfig {
	constructor(private config: ConfigService) {}

	get someVariable(): string {
		return this.config.get('SOME_VARIABLE', true)!;
	}
}
```

## Usage (client)

Server's environment variables which start with `REACT_APP_` prefix will be exposed to the client by serving `/env.js` file. The file will be placed to `CONFIG_PUBLIC_PATH` path on the server.

The best way to access it in the client is to add

```html
<script src="%PUBLIC_URL%/env.js"></script>
```

to the `<head>` section of `index.html`.

This script will create `windows.env` object containing a dictionary of enviroment variables for CRA app.
