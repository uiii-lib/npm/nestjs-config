import { Injectable } from '@nestjs/common';
import fs from 'fs';
import path from 'path';
import dotenv from 'dotenv';

@Injectable()
export class ConfigService {
	constructor() {
		dotenv.config();
		this.publishConfig();
	}

	get(key: string, required: true): string;
	get(key: string, required?: false): string|undefined;
	get(key: string, required?: boolean): string|undefined {
		const value = process.env[key];

		if (value === undefined && required) {
			throw new Error(`Missing environment variable '${key}'`);
		}

		return value;
	}

	protected publishConfig() {
		const publicConfigKeys = Object.keys(process.env).filter(key => key.startsWith('REACT_APP'));
		const publicPath = this.get('CONFIG_PUBLIC_PATH');

		if (publicConfigKeys.length === 0) {
			return;
		}

		if (!publicPath) {
			console.warn("WARNING: There are env variables to be published to the client but CONFIG_PUBLIC_PATH is not set:")
			publicConfigKeys.forEach(key => console.log(`- ${key}`));
			return;
		}

		const output = `
			window.env = window.env || {};
			${publicConfigKeys.map(key => `window.env.${key} = "${process.env[key]}";`).join('\n')}
		`;

		fs.writeFileSync(path.join(publicPath, 'env.js'), output, 'utf-8');
	}
}
